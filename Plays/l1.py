def first_level():
    # Open the ASCII files and put their lines in a list so that they are easier to work with.
    # !! WARNING !! if file is very large, it may cause out of memory error.
    # Need to implement lines limit on files.
    # Written by Ayyzee (https://odysee.com/@sub-pixels:2)
    
    # cache character ASCII
    chatacterASCIIcache = []
    with open('ASCII_Art/characters/Tlatlacoa/neutral.txt') as characterASCII: # A more sleek way of opening files
        for line in characterASCII:
            chatacterASCIIcache.append(line) # Add each line that we read from the file to the list
            
    # cache background ASCII
    backgroundASCIIcache = []
    with open('ASCII_Art/backgrounds_n_menus/field_sheesus.txt') as backgroundASCII:
        for line in backgroundASCII:
            backgroundASCIIcache.append(line)

    # cache text
    number = -8 #variable used to determine which text file should be read
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)

    # cache long line ASCII
    line_longASCIIcache = []
    with open('ASCII_Art/objects/line_long.txt') as line_longASCII:
        for line in line_longASCII:
            line_longASCIIcache.append(line)

    # cache short line ASCII
    line_shortASCIIcache = []
    with open('ASCII_Art/objects/line_short.txt') as line_shortASCII:
        for line in line_shortASCII:
            line_shortASCIIcache.append(line)

            
    
    # Ok this is very dirty but can be improved. It works for now.
    #counter, line    emunerate allows to use the counter.
    for i,line in enumerate(backgroundASCIIcache):
        try:
            #   Move the terminal cursor one character up. then print without line ending. The reason is
            #   because when we will print the character, the lines will wrap. The end='\r' means that we
            #   don't want to use a normal line ending.
            textV = print("\033[C\033[A"+textcache[i], end='\r') # Then print the text
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r') # Then print the short line
            backgroundV = print("\033[A"+line, end='\r') # Print the background ascii BEFORE text
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r') # Then print the character
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r') # Then print the long line
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')   # Print the rest of the background
            except:
                pass
            pass

    print(number)# shows which .txt are you reading (for debbuging purpouses)
   

    # prompt user whether to continue gameplay
    Pi_standard = 'Press <ENTER> to continue> ' # I define a variable to shorten code
    Pi = input(Pi_standard) # input to break the dialoge frames

    # add 1 to text
    number += 1 # adding 1 to number in order to progress dialogue
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    
# SOURCE: https://stackoverflow.com/questions/15599639/what-is-the-perfect-counterpart-in-python-for-while-not-eof

    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)

   
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)
    for i,line in enumerate(backgroundASCIIcache):
        try:
            textV = print("\033[C\033[A"+textcache[i], end='\r')
            line_shortV = print("\033[79C\033[A"+line_shortASCIIcache[i], end='\r')
            backgroundV = print("\033[A"+line, end='\r')
            characterV = print("\033[68C"+chatacterASCIIcache[i], end='\r')
            line_longV = print("\033[79C\033[A"+line_longASCIIcache[i], end='\r')
        except Exception:
            try:
                a = print(backgroundASCIIcache[i+1], end='\r')
            except:
                pass
            pass

    print(number)
    
    Pi = input(Pi_standard)
    
    number += 1 
    textcache = []
    with open(f'Script/l1/{number}.txt') as text:
        for line in text:
            textcache.append(line)

first_level()
