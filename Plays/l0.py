def introduction():
    # prints the text so the dum dum user
    # would know that they have to press <ENTER>
    print('.---------------------|______________________________|-------------------------.')
    print('|                   _/                                \_                       |')
    print('|                   \  Press <ENTER> to see more text  /                       |')
    print('|                    ----------------------------------                        |')
    
    
    # we define a variable
    # we open the file
    fileThatWeOpen = open('ASCII_Art/introduction/first1.txt')
    # then we read it (similar to print)
    fileThatWeOpen = fileThatWeOpen.read()

    # every single line we split it
    for line in fileThatWeOpen.split('\n'):
        # then we pipe it through input
        input(line)

    # starts playing level 1 also known as the game game
    from Plays import l1
        
# after defining the variable we excute it    
introduction()


