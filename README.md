It's a game for the terminal. A simple terminal visual novel for suckless gamers.

# Installation

-1. `git clone https://codeberg.org/TrolerJeans/Rozusvamp`
0. `cd Rozusvamp`
1. `python3 rozusvamp.py`

# Dependencies

Software:V
* python3
* terminal that can display at least 80x7

Hardware:V
* A keyboard
* An output method (preferably monitor, punch cards can work too)
* min. 1Gb of RAM
* min. 8 Gb of space

# Story System

Choices won't impact the endings 
Once you finish the game you'll restart and play again,
Compared to original story begging parts are going be slightly diffirent
As the story progresses more and more story elements aren't going sync up
These alternative stories are going reveal even more about the black sheep
